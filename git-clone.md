### Git clone

Git clone clones a remote repository to your local machine. This comes down to an
exact copy of the remote repository. The default branch is checked out and attached
to the remote branch. So pushing and pulling will work.
