### Git push

Git push pushed local commits to the remote branch. This only works when the local
branch has a remote branch set. If there is not remote branch you will get a hint
on how to set the remote branch.
