### Git pull

Git pull gets the commits from the remote branch and applies them to your local
branch. This way your local branch will be up to date with the remote branch. If
your local branch does not track a remote branch, you will get a hint on how to
set up the remote branch.
