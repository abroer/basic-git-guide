### Git fetch

Git fetch retrieves all the commits from the remote repository and adds them to the
local repository without updating the current HEAD. You will have all commits in
your repository, but your local files will not be updated, because you still look
at the current commit point.
